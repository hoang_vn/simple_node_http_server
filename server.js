const http = require('http');

const config = {
  port:443
}

const server = http.createServer();

server.on('request', (request, response) => {
  let raw_data = Buffer.from('');

  request.on('data', data => {
    raw_data = Buffer.concat([ raw_data, data ]);
  });

  request.on('end', data => {
    if (data) {
      raw_data = Buffer.concat([ raw_data, data ]);
    }

    response.writeHead(200, { 
      'Content-Type': 'application/json'
    });

    setTimeout(() => {
      const result = JSON.stringify({
        data: raw_data.toString(),
        result: {
          ok: true
        }
      });

      response.end(result);

      const log = {
        at: new Date(),
        request: {
          method: request.method,
          url: request.url,
          headers: { ...request.headers },
          data: raw_data
        },
        response: {
          headers: response.getHeaders(),
          data: result,
        }
      }

      console.log(JSON.stringify(log));

    }, 200);
    
  })
});

server.listen(config.port);

console.log(`[INFO] server listening on port ${config.port}`);